<?
include_once 'DB.php';
class CRUD extends DB
{

	function __construct()		{		}
	
	function createQuery($action)
	{
		
		$query=($action=="Agregar")?"insert into ".$this->table." set ":"update ".$this->table." set ";
		$c=array();
		$c=explode("&&&",$_POST['formData']);
		for ($i=1; $i <count($c); $i++) 
		{ 
			$d=str_replace('"','',$c[$i]);
			$d=str_replace('=','="',$d);
			$d=str_replace('+',' ',$d);
			$query.=$d.'"';

			if($i+1<count($c))
				$query.=", ";
		}

		if(strlen($c[0])>3)
			$query.=" where ".$c[0];
			echo $query;
		$this->query($query);
	}
	var $table;

	public function createSelect($tabla,$pk,$campo,$nombre,$idEdit)
	{
		$select="<select class='form-control' name='".$nombre."' >";
		$this->query("select * from ".$tabla." order by ".$campo);

		foreach ($this->bloque as $row)
			$select.="<option value='".$row[$pk]."' ".(($row[$pk]==$idEdit)?"selected":"").">".$row[$campo]."</option>";

		$select.="</select>";
		return $select;
	}


	public function createTable($table,$iconos=array(),$idvi=false,$page=0,$sub='',$cc='info',$cr='success')
	{

		$pl=mysqli_num_rows($this->query($table));

		
		//codigo para paginacion
		$init=1;
		$ls=1000;
		
		$np=ceil($pl/6);
		
		if($page<5)
		{
			$ls=9;
			$init=1;
		}else{
			$ls=$page+6;
			$init=$page-2;
		}
		$this->query($table." limit ".($page*6).", 6");
		$c=mysqli_num_fields($this->bloque);
		$result="";
		if($pl>6)
		{
			$result.='<div>
			<ul class="pagination">
		    <li class="page-link disabled">pagina '.($page+1).' de '.$np.'</li>';
			$result.='<li class="'.($page!=0?'page-item':'dn').' page-item  page-link" onClick="mostrar('.(0).',\'\');">Primera</li>
		    <li onClick="mostrar('.($page-1).',\'\');" class="'.($page==1?'page-item':'dn').' page-item page-link">&laquo;</li>';
		    for ($i=$init; $i <= $np && $i<$ls; $i++) 
				$result.='<li onClick="mostrar('.($i-1).',\'\');"  class="page-link '.($page==$i-1?"active":"").'">'.$i.'</li>';	
		    $result.='<li class="'.(($page+1<$np)?'page-item':'dn').'  page-link" onClick="mostrar('.($page+1).',\'\');">&raquo;</li>
		    <li class="page-item  page-link" onClick="mostrar('.($np-1).',\'\');">Ultima</li>
			</ul>
			</div>
			';
		}
		
		//codigo para crear tabla

		$result.="<table class='table table-hover col-md-12'>";
		//se crea el icono de add, y los espacios para los iconos de acciones
		$result.=  "<thead><tr class='table-".$cc."'>";
		if($iconos!=null)
			$result.=  "<th><button onClick='addRecord".$sub."(\"".$this->table."\")' type='button' class='icon-add btn btn-outline-primary'></button></th>";
		//crea la cabecera con los nombres de las columnas
		for ($j=0; $j <$c ; $j++) 
		{ 
			$campo=mysqli_fetch_field_direct($this->bloque,$j);
			if ($campo->name=='id' && $idvi) 
				$result.=  "<th class='dn'>".$campo->name."</th>";
			else
				$result.=  "<th>".$campo->name."</th>";
		}

		$result.= "</tr></thead><tbody>";
		//crea el contenido de la tabla
		$PoS=false;
		for ($i=0; $i < $this->R ; $i++) 
		{ 
			$result.= "<tr class='table-".(!$PoS?"light":$cr)."'>";//se aplica el color a los renglones
			$PoS=!$PoS;

			$row=mysqli_fetch_array($this->bloque);
			//iconos y acciones para cada registro
			if($iconos!=null)
			{
				$result.="<td width='1%'>";
				foreach ($iconos as $value)
					if($value!="add")
						$result.="<button type='button' onclick='actionbutton(\"".$value."\",\"".$row[0]."\",\"".$this->table."\");' class='icon-$value btn btn-outline-primary m-1 ".($row["avance"]=="100 %"?"dn":"")."'></button>";
				$result.="</td>";
			}
			//informacion dentro de cada campo
			for ($j=0; $j < $c ; $j++) 
			{ 
				$campo=mysqli_fetch_field_direct($this->bloque,$j);
				if ($campo->name=='id' && $idvi) 
					$result.=  "<td class='dn'>".$campo->name."</td>";
				else
					$result.=  "<td>".$row[$j]."</td>";
			}
			$result.= "</tr>";
		}
		$result.=  "</tbody>";
		$result.=  "</table>";
		return $result;
	}

	var $registro="";
	var $action="";
	function formtable($table = '')
	{
		$result="";
		switch($table){
			case 'proyecto':
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>nombre</span></label>".
				"<input type='text' class='form-control ' name='nombre' placeholder='nombre' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['nombre']:"")."' required=''>".
				" </div>";
				$result.="<div class='form-group dn'>".
				"<label class='text-secondary'><span class='icon'>idusuario</span></label>".
				"<input type='text' class='form-control ' name='idusuario' placeholder='idusuario' autocomplete='off' value='".$_SESSION['id']."' required=''>".
				" </div>";
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>Descripcion</span></label>".
				"<textarea class='form-control' name='descripcion'  autocomplete='off'>".(isset($_GET['id'])?$this->registro['descripcion']:"")."</textarea>".
				" </div>";
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>"."estado"."</span></label>".
				$this->createSelect("estado", "id",'descripcion',"idestado",(($this->action=="cached")?$this->registro["idestado"]:0)).
				"</div>";
			break;
			case 'categoria':
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>categoria</span></label>".
				"<input type='text' class='form-control ' name='categoria' placeholder='categoria' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['categoria']:"")."' required=''>".
				" </div>";
			break;
			case 'usuario':
				$result.="<div class='form-group dn'>".
				"<label class='text-secondary'><span class='icon'>ultima</span></label>".
				"<input type='text' class='form-control ' name='ultima' placeholder='ultima' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['ultima']:"")."' required=''>".
				" </div>";
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>usuario</span></label>".
				"<input type='text' class='form-control ' name='usuario' placeholder='usuario' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['usuario']:"")."' required=''>".
				" </div>";
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>correo</span></label>".
				"<input type='email' class='form-control ' name='correo' placeholder='correo' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['correo']:"")."' required=''>".
				" </div>";
			break;
			case 'actividade':
				if($_SESSION['tipo']==1)
				{
					$result.="<div class='form-group'>".
					"<label class='text-secondary'><span class='icon'>Usuario a asignar</span></label>".
					$this->createSelect("usuario", "id",'usuario',"idusuario",(($this->action=="cached")?$this->registro["idusuario"]:0)).
					"</div>";
					$result.="<div class='form-group dn'>".
					"<label class='text-secondary'><span class='icon'>fecha</span></label>".
					"<input type='text' class='form-control ' name='fecha' placeholder='fecha' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['fecha']:Date('Y-m-d H:i:s'))."' required=''>".
					" </div>";
					$result.="<div class='form-group'>".
					"<label class='text-secondary'><span class='icon'>proyecto</span></label>".
					$this->createSelect("proyecto", "id",'nombre',"idproyecto",(($this->action=="cached")?$this->registro["idproyecto"]:1)).
					" </div>";
				}
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>proceso</span></label>".
				"<input type='range' class='form-control ' name='proceso' value='".(isset($_GET['id'])?$this->registro['proceso']:0)."' list='tickmarks'>
				<datalist id='tickmarks'>
				  <option value='0' label='0%'>
				  <option value='10'>
				  <option value='20'>
				  <option value='30'>
				  <option value='40'>
				  <option value='50' label='50%'>
				  <option value='60'>
				  <option value='70'>
				  <option value='80'>
				  <option value='90'>
				  <option value='100' label='100%'>
				</datalist>".
				" </div>";
				//ultima
				$result.="<div class='form-group'>".
				"<label class='text-secondary'><span class='icon'>Descripcion</span></label>".
				"<textarea class='form-control' name='descripcion'  autocomplete='off'>".(isset($_GET['id'])?$this->registro['descripcion']:"")."</textarea>".
				" </div>";
				break;
			}
			return $result;
		}

	function accion($action)
	{
		$this->action=$action;
		switch ($action) 
		{
			case 'trash':
				$this->query("delete from ".$this->table." where id=".$_GET['id']);
			break;
		
			case 'briefcase1':
				echo "lorem";
			break;
			case 'hand':
				$this->query("update ".$this->table." set idusuario=".$_SESSION['id']." where id=".$_GET['id']);
			break;
			case 'check':
				$this->query("update ".$this->table." set proceso=100 where id=".$_GET['id']);
			break;
			case 'cancel4':
				$this->query("update ".$this->table." set idusuario=1 where id=".$_GET['id']);
			break;
			case 'cached':
				$this->registro=$this->sacaTupla("select * from ".$this->table." where id=".$_GET['id']);

			case 'new':
				$this->query("select * from ".$this->table." limit 1");
				
					$result="<div class='row center'>
						  <form id='formdata' class='cb col-12' method='POST' action='#'>
						  <h3>".(($action=="new")?"Agregar":"Actualizar")."</h3>
					    <fieldset class='alert'>
					    <input id='table' type='hidden' value='".$this->table."'>
					    <input id='action' type='hidden' value='".(($action=="new")?"Agregar":"Actualizar")."'>
						";
					$result.="<div class='form-group dn'>".
					"<label class='text-secondary'><span class='icon'>id</span></label>".
					"<input type='text' class='form-control ' name='id' placeholder='id' autocomplete='off' value='".(isset($_GET['id'])?$this->registro['id']:"")."'>".
					" </div>".$this->formtable($this->table);
					
					//		$result.="<div class='form-group'><label class='text-secondary'><span class='icon'>"."estado"."</span></label>".$this->createSelect("estado", $FK['campPrimario'],'descripcion',$FK['campForaneo'],(($action=="cached")?$this->registro[$FK['campForaneo']]:0))."</div>";
				
				$result.='<button onClick="addupd(\''.$this->table.'\',\''.(($action=="new")?"Agregar":"Actualizar").'\');" type="button" class="emerg btn btn-primary btn-lg btn-block">'.(($action=="new")?"Agregar":"Actualizar").'</button>'."</form></div>";
				echo $result;
			break;
			default:
				echo "<script>location.replace(".$this->table.")</script>";
		}
	}
}
$oCO=new CRUD();
$oCO->connect();
?>