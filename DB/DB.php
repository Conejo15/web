<?
date_default_timezone_set("America/Mexico_city");//cuando usas funciones del servidor de Ej: Date('Y-m-d H:i:s') hace que te de la hora de donde estas,
												//el servidor aveces es de otro pais y su hora es diferente
function newPassword()//generara una contraseña 
{
	$cadena="ABCDEFGHIJKLMNPQRSTUVWXYZ123456789123456789";//caracteres validos para la contraseña se ponen dos veces los numeros para equilibrar letras y numeros
	$numeC=strlen($cadena);
	$nuevPWD="";
	for ($i=0; $i<8; $i++)
	  $nuevPWD.=$cadena[rand()%$numeC]; //las cadenas en php se pueden manejar como arreglos
	return $nuevPWD;//cadena creada aleatoriamente
}
function Errores($caso)//muestra los errores es un catalago
{
	switch ($caso)
	{
		case '1':
			return "USUARIO Y/O CONTRASEÑA SON ERRONEOS";
			break;
		case '2':
			return "ERROR AL MANDAR EL CORREO SU CONTRASEÑA ES: ".$_GET['pwd'];
			break;
		case '3':
			return "SU CONTRASEÑA FUE MANDADA AL CORREO QUE PROPORCIONO";
			break;
		case '4':
			return "EL CORREO QUE PROPORCIONO YA ESTA ASOCIADO A UNA CUENTA";
			break;
		case '5':
			return "LA CUENTA QUE QUIERE RECUPERAR NO EXISTE";
			break;
		case '6':
			return "DEBE INICIAR SESION ANTES";
			break;
		
		default:
			return "SE PUEDE REGISTRAR, INICIAR SESION O RECUPERAR SU CUENTA";
			break;
	}
}
class DB
{
	var $link;//conexion
	var $bloque;//bloque de datos
	var $R;//renglones o numero de registros traidos
	var $numCampos;//numero de columnas
	var $table;//nombre de la tabla a trabajar

	function __construct()	{		}

	function setTable($table='')	//asigno la tabla con la que trabajare
	{
		$this->table=$table;		
	}

	public function connect()//realiza la conexion con la base de datos
	{								//servidor	usuario	contraseña	base de datos
		//$this->link = mysqli_connect('servidor','usuario','password','base de datos');
		$this->link = mysqli_connect('localhost','root','conejo15','gp');
	}

	public function query($query)//ejecuta la query
	{
		if (strpos(strtoupper($query), "SELECT")!==false)//si es una consulta 
		{
			$this->bloque=mysqli_query($this->link,$query);//la ejecuta y se trae todos los registros
			$this->R=mysqli_num_rows($this->bloque);//numero de renglones o de registros
			$this->numCampos=mysqli_num_fields($this->bloque);//numero de columnas
			return $this->bloque;
		}
		else
			mysqli_query($this->link,$query);//si no, solo ejecuta el update, delete, insert
	}

	public function camposData($indice)//este funcionara para crear tablas automaticas 
	{
		return mysqli_fetch_field_direct($this->bloque,$indice);//se trae el nombre del campo
	}
	
	public function sacaTupla($query)//regresa un solo registro
	{		
		$row=mysqli_query($this->link, $query);//la ejecuta
		if (mysqli_num_rows($row)) //si exiten registros
			return mysqli_fetch_array($row);//los regresa, por lo general solo regresa 1 solo registro
		else
			return false;//regresa que esta vacias
	}
	public function close()//cierra la conexion
	{
		mysqli_close($this->link);//la cierra
	}
	
	public function createArray($table)
	{
		$this->query($table);
		$data=Array();
		while($row=mysqli_fetch_array($this->bloque))
			$data[]=$row;
		return json_encode($data);
	}	
}


$oDB=new DB();//como se trabaja con clases se crean objetos
//strpos("cadena en donde buscaras","lo que vas a buscar") regresa la posicion en donde se encuentra la cadena a buscar si no la encuentra regresa un -1
//strtoupper("cadena que hara mayusculas") REGRESARA LA CADENA EN MAYUSCULAS
$oDB->connect();
?>
