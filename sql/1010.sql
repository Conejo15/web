/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80017
Source Host           : localhost:3306
Source Database       : gp

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2019-10-10 11:56:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `areas`
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `IdProyecto` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProyecto` (`IdProyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of areas
-- ----------------------------

-- ----------------------------
-- Table structure for `catalogo_estatus`
-- ----------------------------
DROP TABLE IF EXISTS `catalogo_estatus`;
CREATE TABLE `catalogo_estatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of catalogo_estatus
-- ----------------------------
INSERT INTO `catalogo_estatus` VALUES ('1', 'No Asignado');
INSERT INTO `catalogo_estatus` VALUES ('2', 'RevisiÃ³n');
INSERT INTO `catalogo_estatus` VALUES ('3', 'Prob. Alternativas');
INSERT INTO `catalogo_estatus` VALUES ('4', 'solucionado');

-- ----------------------------
-- Table structure for `categoria`
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordenar` (`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES ('14', 'asd');
INSERT INTO `categoria` VALUES ('16', 'asd');
INSERT INTO `categoria` VALUES ('18', 'asdadf');
INSERT INTO `categoria` VALUES ('15', 'buu buuu buu');
INSERT INTO `categoria` VALUES ('17', 'njk  nkml');
INSERT INTO `categoria` VALUES ('13', 'wZEsrxdctfvyg xrctvygbuh');

-- ----------------------------
-- Table structure for `persona`
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persona
-- ----------------------------

-- ----------------------------
-- Table structure for `personaproyecto`
-- ----------------------------
DROP TABLE IF EXISTS `personaproyecto`;
CREATE TABLE `personaproyecto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProyecto` int(11) NOT NULL,
  `IdPersona` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IdProyecto_2` (`IdProyecto`,`IdPersona`),
  KEY `IdProyecto` (`IdProyecto`),
  KEY `IdPersona` (`IdPersona`),
  CONSTRAINT `personaproyecto_ibfk_2` FOREIGN KEY (`IdPersona`) REFERENCES `persona` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of personaproyecto
-- ----------------------------

-- ----------------------------
-- Table structure for `problema`
-- ----------------------------
DROP TABLE IF EXISTS `problema`;
CREATE TABLE `problema` (
  `Id` int(11) NOT NULL,
  `Descripcion` text NOT NULL,
  `Estatus` int(11) NOT NULL DEFAULT '1',
  `Observaciones` text NOT NULL,
  `idPersona` int(11) NOT NULL,
  `Fecha_registro` date NOT NULL,
  `Fecha _solucion` date NOT NULL,
  `IdArea` int(11) NOT NULL,
  KEY `Estatus` (`Estatus`),
  KEY `IdArea` (`IdArea`),
  CONSTRAINT `problema_ibfk_1` FOREIGN KEY (`IdArea`) REFERENCES `areas` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `problema_ibfk_2` FOREIGN KEY (`Estatus`) REFERENCES `catalogo_estatus` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of problema
-- ----------------------------

-- ----------------------------
-- Table structure for `proyecto`
-- ----------------------------
DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE `proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordnombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Records of proyecto
-- ----------------------------
INSERT INTO `proyecto` VALUES ('9', 'f', null);
INSERT INTO `proyecto` VALUES ('11', 'g', null);
INSERT INTO `proyecto` VALUES ('12', 'i', null);
INSERT INTO `proyecto` VALUES ('13', 'j', null);
INSERT INTO `proyecto` VALUES ('14', 'k', null);
INSERT INTO `proyecto` VALUES ('15', 'o', null);
INSERT INTO `proyecto` VALUES ('16', 'l', null);
INSERT INTO `proyecto` VALUES ('17', 'p', null);
INSERT INTO `proyecto` VALUES ('18', 'qwerty', null);
INSERT INTO `proyecto` VALUES ('19', 'Sistema20CAM%2520de%2520%2520Safre%20%20%20%20%20%20%20ddfdfdfd', null);
INSERT INTO `proyecto` VALUES ('20', 'poihjkbn', 'fcghjbklm');
INSERT INTO `proyecto` VALUES ('22', 'asdfsadf', 'sdaf%20asdf');
INSERT INTO `proyecto` VALUES ('23', 'asdas', 'asd');
INSERT INTO `proyecto` VALUES ('24', 'qawsedrftgy%20cfvgbhn', 'xdcfvghb%20vbhnjmk%20');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT '1',
  `usuario` varchar(255) DEFAULT NULL,
  `correo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'user.png',
  `ultima` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correoindex` (`correo`),
  KEY `ordnombre` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('23', '1', 'hugo', 'hpalaziozhugo%40gmail.com', '8087cc0382a8d2100df6c07468a86491', 'user.png', '0000-00-00 00:00:00');
INSERT INTO `usuario` VALUES ('24', '2', 'Ferminas', 'fermin%2540fermin.com', 'qwerty', '', '2019-10-10 10:32:31');

-- ----------------------------
-- Function structure for `addUser`
-- ----------------------------
DROP FUNCTION IF EXISTS `addUser`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `addUser`(usname varchar(40),email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 RETURN 0;
	ELSE
		INSERT into usuarios(usuario,correo,pwd)values(usname,email,passwd);
		RETURN 1;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `uppass`
-- ----------------------------
DROP FUNCTION IF EXISTS `uppass`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `uppass`(email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 update usuarios set pwd=passwd where correo=email;
		return 1;
	ELSE
		RETURN 0;
	end if;
END
;;
DELIMITER ;
