SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `areas`
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `IdProyecto` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProyecto` (`IdProyecto`)
);

-- ----------------------------
-- Records of areas
-- ----------------------------

-- ----------------------------
-- Table structure for `catalogo_estatus`
-- ----------------------------
DROP TABLE IF EXISTS `catalogo_estatus`;
CREATE TABLE `catalogo_estatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
);

-- ----------------------------
-- Records of catalogo_estatus
-- ----------------------------
INSERT INTO `catalogo_estatus` VALUES ('1', 'No Asignado');
INSERT INTO `catalogo_estatus` VALUES ('2', 'RevisiÃ³n');
INSERT INTO `catalogo_estatus` VALUES ('3', 'Prob. Alternativas');
INSERT INTO `catalogo_estatus` VALUES ('4', 'solucionado');

-- ----------------------------
-- Table structure for `persona`
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
);

-- ----------------------------
-- Records of persona
-- ----------------------------

-- ----------------------------
-- Table structure for `personaproyecto`
-- ----------------------------
DROP TABLE IF EXISTS `personaproyecto`;
CREATE TABLE `personaproyecto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProyecto` int(11) NOT NULL,
  `IdPersona` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IdProyecto_2` (`IdProyecto`,`IdPersona`),
  KEY `IdProyecto` (`IdProyecto`),
  KEY `IdPersona` (`IdPersona`),
  CONSTRAINT `personaproyecto_ibfk_1` FOREIGN KEY (`IdProyecto`) REFERENCES `proyecto` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `personaproyecto_ibfk_2` FOREIGN KEY (`IdPersona`) REFERENCES `persona` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ----------------------------
-- Records of personaproyecto
-- ----------------------------

-- ----------------------------
-- Table structure for `problema`
-- ----------------------------
DROP TABLE IF EXISTS `problema`;
CREATE TABLE `problema` (
  `Id` int(11) NOT NULL,
  `Descripcion` text NOT NULL,
  `Estatus` int(11) NOT NULL DEFAULT '1',
  `Observaciones` text NOT NULL,
  `idPersona` int(11) NOT NULL,
  `Fecha_registro` date NOT NULL,
  `Fecha _solucion` date NOT NULL,
  `IdArea` int(11) NOT NULL,
  KEY `Estatus` (`Estatus`),
  KEY `IdArea` (`IdArea`),
  CONSTRAINT `problema_ibfk_1` FOREIGN KEY (`IdArea`) REFERENCES `areas` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `problema_ibfk_2` FOREIGN KEY (`Estatus`) REFERENCES `catalogo_estatus` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ----------------------------
-- Records of problema
-- ----------------------------

-- ----------------------------
-- Table structure for `proyecto`
-- ----------------------------
DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE `proyecto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(70) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `areas` (`IdProyecto`) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ----------------------------
-- Records of proyecto
-- ----------------------------

-- ----------------------------
-- Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `correo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ultima` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', '1', 'conejo', 'a@a.com', '31f466af75370e2dec5a2c17565a03ed', 'conejo.png', '2019-09-23 23:08:37');
INSERT INTO `usuarios` VALUES ('7', '1', 'conejo', 'hpalazioz_hugo@hotmail.com', '31f466af75370e2dec5a2c17565a03ed', null, '2019-09-23 23:10:47');

-- ----------------------------
-- Function structure for `addUser`
-- ----------------------------
--SI NO EXISTE SU CORREO YA REGISTRADO LO REGISTRA
DROP FUNCTION IF EXISTS `addUser`;
DELIMITER ;;
CREATE FUNCTION `addUser`(usname varchar(40),email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 RETURN 0;
	ELSE
		INSERT into usuarios(usuario,correo,pwd)values(usname,email,passwd);
		RETURN 1;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `uppass`
-- ----------------------------
--ACTUALIZA LA CONTRASE;A DE LOS USUARIOS DE LOS CUALES SI EXISTE SU CORREO
DROP FUNCTION IF EXISTS `uppass`;
DELIMITER ;;
CREATE FUNCTION `uppass`(email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 update usuarios set pwd=passwd where correo=email;
		return 1;
	ELSE
		RETURN 0;
	end if;
END
;;
DELIMITER ;
