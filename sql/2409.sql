/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80017
Source Host           : localhost:3306
Source Database       : gp

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2019-09-24 10:45:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `areas`
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `IdProyecto` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProyecto` (`IdProyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of areas
-- ----------------------------

-- ----------------------------
-- Table structure for `catalogo_estatus`
-- ----------------------------
DROP TABLE IF EXISTS `catalogo_estatus`;
CREATE TABLE `catalogo_estatus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of catalogo_estatus
-- ----------------------------
INSERT INTO `catalogo_estatus` VALUES ('1', 'No Asignado');
INSERT INTO `catalogo_estatus` VALUES ('2', 'RevisiÃ³n');
INSERT INTO `catalogo_estatus` VALUES ('3', 'Prob. Alternativas');
INSERT INTO `catalogo_estatus` VALUES ('4', 'solucionado');

-- ----------------------------
-- Table structure for `persona`
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of persona
-- ----------------------------

-- ----------------------------
-- Table structure for `personaproyecto`
-- ----------------------------
DROP TABLE IF EXISTS `personaproyecto`;
CREATE TABLE `personaproyecto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdProyecto` int(11) NOT NULL,
  `IdPersona` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IdProyecto_2` (`IdProyecto`,`IdPersona`),
  KEY `IdProyecto` (`IdProyecto`),
  KEY `IdPersona` (`IdPersona`),
  CONSTRAINT `personaproyecto_ibfk_1` FOREIGN KEY (`IdProyecto`) REFERENCES `proyecto` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `personaproyecto_ibfk_2` FOREIGN KEY (`IdPersona`) REFERENCES `persona` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of personaproyecto
-- ----------------------------

-- ----------------------------
-- Table structure for `problema`
-- ----------------------------
DROP TABLE IF EXISTS `problema`;
CREATE TABLE `problema` (
  `Id` int(11) NOT NULL,
  `Descripcion` text NOT NULL,
  `Estatus` int(11) NOT NULL DEFAULT '1',
  `Observaciones` text NOT NULL,
  `idPersona` int(11) NOT NULL,
  `Fecha_registro` date NOT NULL,
  `Fecha _solucion` date NOT NULL,
  `IdArea` int(11) NOT NULL,
  KEY `Estatus` (`Estatus`),
  KEY `IdArea` (`IdArea`),
  CONSTRAINT `problema_ibfk_1` FOREIGN KEY (`IdArea`) REFERENCES `areas` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `problema_ibfk_2` FOREIGN KEY (`Estatus`) REFERENCES `catalogo_estatus` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of problema
-- ----------------------------

-- ----------------------------
-- Table structure for `proyecto`
-- ----------------------------
DROP TABLE IF EXISTS `proyecto`;
CREATE TABLE `proyecto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(70) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `areas` (`IdProyecto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proyecto
-- ----------------------------

-- ----------------------------
-- Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT '1',
  `usuario` varchar(255) DEFAULT NULL,
  `correo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'user.png',
  `ultima` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correoindex` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', '1', 'Conejo Palacios', 'a@a.com', '31f466af75370e2dec5a2c17565a03ed', '1.png', '2019-09-24 10:34:56');
INSERT INTO `usuarios` VALUES ('16', '1', 'conejito', 'hpalazioz_hugo@hotmail.com', '31f466af75370e2dec5a2c17565a03ed', null, '2019-09-24 09:55:00');

-- ----------------------------
-- Function structure for `addUser`
-- ----------------------------
DROP FUNCTION IF EXISTS `addUser`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `addUser`(usname varchar(40),email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 RETURN 0;
	ELSE
		INSERT into usuarios(usuario,correo,pwd)values(usname,email,passwd);
		RETURN 1;
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `uppass`
-- ----------------------------
DROP FUNCTION IF EXISTS `uppass`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `uppass`(email varchar(40),passwd varchar(40)) RETURNS int(4)
BEGIN
	DECLARE cant int(4);
	set cant=(select count(*) from usuarios where correo=email);
	IF cant>0
	THEN
	 update usuarios set pwd=passwd where correo=email;
		return 1;
	ELSE
		RETURN 0;
	end if;
END
;;
DELIMITER ;
