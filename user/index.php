<?session_start();?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
  <? include 'cabecera.php';?>
	<title>Gestor de Proyectos</title>
</head>
<body>
	<?include_once 'menu.php';?>
	<div class="container">
		<div class="row mt-5">
			<div class="col-md-3">
				<div class="col-md-12">
					<div class="list-goup">
						<div id="card">
							<div class="card">
								<div class="card-header">
									<a class="card-link collapsed" data-toggle="collapse" data-parent="#card-910662" href="#card-element-265252"><h4 class="text-center">Menu</h4></a>
								</div>
								<div id="card-element-265252" class="collapse show">
									<span onClick="yotengo();" id="yo" class="listactivi list-group-item list-group-item-action">Mis actividades</span>
									<span onClick="sindueno();" id="sin" class="listactivi list-group-item list-group-item-action">Actividades sin asignar</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<div class="col-md-9">
				<div class="jumbotron">
					<h1 id="tema"></h1>
					<div id="mostrartabla"></div>
				</div>
			</div>
		</div>
	</div>

<div class="modalmsg" id="modalmsg"><span class="cerrar icon-cross" onClick="cerrar();"></span><div class="jumbotron modalcontainer"><div id="contenidomodal"></div></div></div>
<script>

function yotengo()
{
	$('.listactivi').removeClass('active');
	$('#yo').addClass('active');
	$('#tema')[0].innerHTML='Tus Actividades';
	tquery="Select ac.id, p.nombre, ac.descripcion,fecha, concat(proceso,' %') avance  from actividade ac inner join proyecto p on p.id=ac.idproyecto where ac.idusuario=<?echo $_SESSION['id'];?>";	
	ticonos=["check","cancel4","cached"];
	ttable="actividade";
	mostrar(0,"");
}

function sindueno()
{
	$('.listactivi').removeClass('active');
	$('#sin').addClass('active');
	$('#tema')[0].innerHTML='Actividades no asignadas';
	tquery="Select ac.id, p.nombre, ac.descripcion,fecha,concat(proceso,' %') avance  from actividade ac inner join proyecto p on p.id=ac.idproyecto where ac.idusuario=1";	
	ticonos=["hand"];
	ttable="actividade";
	mostrar(0,"");
}
yotengo();

</script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>