<?session_start();?>
<script>
</script>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Site</title>
	<?  include 'cabecera.php';  ?>

</head>
<body>
  <?
  include_once '../DB/DB.php';
  include 'menu.php';
if(!isset($_SESSION['correo']))//checamos si esta logueado
  header('location: ../index.php?e=6');//si no lo redirigimos al login

if(isset($_POST['pwdA']))//checamos si se mando la contrase;a actual, la cual pido para comprobar cambios
if(md5($_POST['pwdA'])==$_SESSION['password'])//si son iguales la actual con la que ingreso, se generaran cambios
{
  $foto='';
  if(isset($_FILES['fotoN']) && $_FILES['fotoN']['name']>"") //checamos si mando la foto
  {
    //si mando la foto la guardamos en nuestra carpeta de imagenes
    $r=explode(".", $_FILES['fotoN']['name']);

    $foto= $_SESSION['id'].".".$r[count($r)-1];//guardamos la ruta para la base de datos

    copy($_FILES['fotoN']['tmp_name'],"../img/".$foto);//la copiamos, si la movemos aveces genera problemas de permisos
  }

  
    $qup="update usuario set usuario='".$_POST['userN']."' ";

    if(isset($_POST['camcon']) && $_POST['pwdN']==$_POST['pwdN2'])//si mi usuario quiere cambiar la contrase;a
      {//si es asi se actualizara y se guardara la nueva contrase;a en las sesiones
        $qup.=", pwd='".md5($_POST['pwdN'])."'";
        $_SESSION['password']=md5($_POST['pwdN']);
      }

    if($foto!='')//verifica si existe una nueva ruta para la imagen
      {//si es asi se actualizara  y se guardara la foto
        $qup.=", foto='".$foto."'";
        $_SESSION['foto']=$foto;
      }
    $oDB->query($qup." where id=".$_SESSION['id']);//ejecutamos cambios
    $_SESSION['usuario']=$_POST['userN'];//guardamos en las sesiones el nuevo nombre
    $oDB->close();//cerramos la conexion a la base de datos
}
/**/
?>
<div class="container">
  <div class="row center">
  <div class="col-3"></div>
  <form class="jumbotron cb mt-5 mb-5 col-6 alert " name="perf" method="POST" enctype="multipart/form-data">
<h1 id="hh">Actualizar datos</h1>
      <div class="form-group">
        <label class="text-secondary" ><span class="icon-user"></span>Nombres</label>
        <input type="text" class="form-control" name="userN" value="<?echo $_SESSION['usuario']?>" autocomplete="off">
      </div>
      <div class="form-group">
        <label class="text-secondary" ><span class="icon-lock"></span>Contraseña</label>
        <input type="text" class="form-control" id="pass"  name="pwdA" placeholder="Contraseña" autocomplete="off" required>
      </div>
      <div class="form-group">
        <!-- si quiere cambiar la contrase;a -->
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" name="camcon" id="customCheck1">
          <label class="custom-control-label" for="customCheck1">¿Cambiar Contraseña?</label>
        </div>
      </div>
      <!-- pide una nueva contrase;a -->
      <div class="form-group dn" id="pas1">
        <label class="text-secondary" ><span class="icon-lock"></span>Nueva Contraseña</label>
        <input type="password" class="form-control checkp" id="pass1"  name="pwdN" placeholder="Contraseña" autocomplete="off">
      </div>
      <!-- pide  la confirmacion de ella-->
      <div class="form-group has-success dn" id="pas2">
        <label class="text-secondary" ><span class="icon-lock"></span>Confirmar</label>
        <input type="password" class="form-control checkp"   id="pass2" name="pwdN2" placeholder="Confirme Contraseña" autocomplete="off">
        <!-- hice una validacion mas dinamica para saber si son igales o no -->
        <div class="valid-feedback">Las contraseñas coinciden <span class="icon-check"></span></div>
        <div class="invalid-feedback">Las contraseñas no coinciden <span class="icon-error"></span></div>
      </div>
    <!-- input para la imagen -->
      <div class="form-group">
        <div class="input-group mb-3">
          <div class="custom-file">
            <input type="file" class="custom-file-input " name="fotoN" id="files" autocomplete="off" accept=".jpg, .jpeg,.png">
            <label class="custom-file-label " id="imglabel" for="inputGroupFile02"><span class="icon-image is-valid"></span> Foto de perfil </label>
          </div>
        </div>
        <!-- donde se mostrara una vista previa de la nueva imagen que quiere -->
        <div class="input-group col-6" id="list"><img class="imgperfil" src="../img/<?echo $_SESSION['foto']."?".(rand(1,1000));?>"></div>
      </div>
      <button type="submit" id="btnGuardar" class="btn btn-primary w-100"> <span class="icon-input">Actualizar</span></button>
        
  </form>
    <script>
      //validara si las nuevas contrase;as son iguales
      $(".checkp").on("change paste keyup",function () {
          var txtpass1=document.getElementById("pass1");
          var txtpass2=document.getElementById("pass2");

          if(pass2.value==pass1.value &&pass1.value!="")
          {
            pass2.classList.add("is-valid");
            pass2.classList.remove("is-invalid");
            $("#btnGuardar").prop("disabled",false);
          }else{
            $("#btnGuardar").prop("disabled", true);
            pass2.classList.add("is-invalid");
            pass2.classList.remove("is-valid");
          }
         
        });
      //si quiere cambiar de contrase;a mostrara los nuevos campos
        $("#customCheck1").on("click change",function ()
        {
          if(customCheck1.checked)
          {
            $("#btnGuardar").prop("disabled", true);
            pas2.classList.remove("dn");
            pas1.classList.remove("dn");
          }else{
            $("#btnGuardar").prop("disabled",false);
            pas1.classList.add("dn");
            pas2.classList.add("dn");
          }

        });
        //esto hace que se vea la nueva imagen 
    function archivo(evt) 
    {
      var files = evt.target.files; 
      var f=files[0];
      var reader = new FileReader();
      reader.onload = (function(theFile) 
      {
        return function(e) 
        {
          document.getElementById("list").innerHTML = ['<img class="imgperfil" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById("imglabel").innerHTML=perf[6].files[0].name;
        };
      })(f);
      reader.readAsDataURL(f);
    }
  document.getElementById('files').addEventListener('change', archivo);
</script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>
