<?session_start();?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
  <? include 'cabecera.php';?>
	<title>Gestor de Proyectos</title>
</head>
<body>
	<?include_once 'menu.php';?>
	<div class="container">
	<div class="row">
		<div class="col-md-12 mt-5">
			<div class="jumbotron">
				<h1 id="tema"></h1>
				<div id="mostrartabla"></div>
			</div>
		</div>
	</div>
</div>

<div class="modalmsg" id="modalmsg"><span class="cerrar icon-cross" onClick="cerrar();"></span><div class="jumbotron modalcontainer"><div id="contenidomodal"></div></div></div>
<script>
	
iconos=["add","trash","cached"];
mostrar("proyecto",iconos);

</script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>