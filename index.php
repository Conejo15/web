<?session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
  <? include 'cabecera.php';
    include_once 'DB/DB.php';
    include_once 'verifica.php';

    if(isset($_SESSION['correo']))
      if($_SESSION['tipo']==1)
            header("Location:admin/");
          elseif($_SESSION['tipo']==2)
            header("Location:user/");
            else
              header("Location:./?e=0");
    ?>
	<title>Document</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">CSC</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Login <span class="sr-only"></span></a>
        </li>
      </ul>
    </div>
  </nav>
      <div class="alert alert-dismissible alert-danger ta-c">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?echo Errores(isset($_GET['e'])?$_GET['e']:"0");?></strong>
      </div>
  <div class="row">
  	<div class="col-4"></div>
    <div class="jumbotron m-5 col-4">
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#login">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#registrar">Registrar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#recuperar">Recuperar</a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade show active" id="login">
          <form method="POST">
            <input type="hidden" name="action" value="1">
            <div class="form-group">
                <label class="col-form-label col-form-label-lg" for="inputLarge">Correo</label>
                <input class="form-control form-control-lg" name="email" value="hpalaziozhugo@gmail.com" type="email">
            </div>
            <div class="form-group">
                <label class="col-form-label col-form-label-lg" for="inputLarge">Contraseña</label>
                <input class="form-control form-control-lg" name="pwd" value="conejo"  type="password">
            </div>
            <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
          </form>
        </div>
        <div class="tab-pane fade" id="registrar">
          <form method="POST">
            <input type="hidden" name="action" value="2">
            <div class="form-group">
                <label class="col-form-label col-form-label-lg" for="inputLarge">Usuario</label>
                <input class="form-control form-control-lg" name="user" type="text">
            </div>
            <div class="form-group">
                <label class="col-form-label col-form-label-lg" for="inputLarge">Correo</label>
                <input class="form-control form-control-lg" name="email" type="email">
            </div>

            <button type="submit" class="btn btn-primary btn-lg btn-block">Registrar</button>
          </form>
        </div>
        <div class="tab-pane fade" id="recuperar">
          <form method="POST">
            <input type="hidden" name="action" value="3">
            <div class="form-group">
                <label class="col-form-label col-form-label-lg" for="inputLarge">Correo</label>
                <input class="form-control form-control-lg" name="email" type="email">
            </div>
            <button type="submit" class="btn btn-primary btn-lg btn-block">Recupera</button>
          </form>
        </div>
      </div>
    </div>
  </div>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>